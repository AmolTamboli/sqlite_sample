//
//  SignupModel.swift
//  SQLlite Demo
//
//  Created by Amol Tamboli on 23/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import Foundation

struct SignupModel {
    let fname:String
    let lname:String
    let phone:String
    let email:String
}
