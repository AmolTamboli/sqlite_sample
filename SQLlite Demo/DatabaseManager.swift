//
//  DatabaseManager.swift
//  SQLlite Demo
//
//  Created by Amol Tamboli on 23/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import Foundation

var shareInstance = DatabaseManager()

class DatabaseManager: NSObject {
    
    var database : FMDatabase? = nil
    
    class func getInstance() -> DatabaseManager{
        if shareInstance.database == nil {
            shareInstance.database = FMDatabase(path: Util.getPath("Signup.db"))
        }
        return shareInstance
    }
    
    func saveData(_ modelInfo:SignupModel) -> Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("INSERT INTO Signup (fname,lname,phone,email) VALUES(?,?,?,?)", withArgumentsIn: [modelInfo.fname,modelInfo.lname,modelInfo.phone,modelInfo.email])
        shareInstance.database?.close()
        return isSave!
    }
}
