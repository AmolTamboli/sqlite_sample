//
//  ViewController.swift
//  SQLlite Demo
//
//  Created by Amol Tamboli on 23/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnClickedSave(_ sender: Any) {
        let modelInfo = SignupModel(fname: txtFirstName.text!, lname: txtLastName.text!, phone: txtPhone.text!, email: txtEmail.text!)
        let isSave = DatabaseManager.getInstance().saveData(modelInfo)
        print(isSave)
    }
    
}

