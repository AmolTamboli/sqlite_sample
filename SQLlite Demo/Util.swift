//
//  Util.swift
//  SQLlite Demo
//
//  Created by Amol Tamboli on 23/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import Foundation

class Util: NSObject {
    
    //MARK:- Get Document Directory Path
    class func getPath(_ fileName : String) -> String{
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileUrl = documentDirectory.appendingPathComponent(fileName)
        print("Document Path:- \(fileUrl.path)")
        return fileUrl.path
    }
    
    //MARK:- Get Bundle Directory Path
    class func copyDatabase(_ fileName : String) {
        let dbPath = getPath("Signup.db")
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: dbPath){
            let bundle = Bundle.main.resourceURL
            let file = bundle?.appendingPathComponent(fileName)
            var error:NSError?
            do {
                try fileManager.copyItem(atPath: file!.path, toPath: dbPath)
            } catch let error1 as NSError{
                error = error1
            }
            
            if error == nil {
                //MARK:- Show alert OR pop-up
                print("Error in DB")
            } else {
                print("Yeah !!")
            }
        }
    }
}
